This project is developed in the Model-View-Presenter pattern.

APK : https://www.dropbox.com/s/xfqtb2v6kzj1bkc/SCB_Assignment.apk

# Library #
* Dagger2
* RxJava

# Timeline #

* 14/05/2017 12:30 init AndroidProject
* 14/05/2017 14:00 break
* 14/05/2017 15:30 start implement domain&data modules
* 14/05/2017 17:00 break
* 15/05/2017 08:00 start implement presentation module
* 15/05/2017 10:00 finish
