package th.in.santi.data.datasource;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import th.in.santi.domain.interactor.BallsUseCase;

/**
 * Created by Santi Tepinjai on 14-May-2017.
 * @see {https://bitbucket.org/SantiDev/scb-assignment}
 */

@Singleton
public class BallsDataStoreFactory implements BallsDataSource {

    private final Context context;

    @Inject
    public BallsDataStoreFactory(Context context) {
        this.context = context;
    }

    @Override
    public Observable<BallsUseCase.ResponseValues> operateBalls(BallsUseCase.RequestValues requestValues) {
        return new BallsLocalDataSource(context).operateBalls(requestValues);
    }
}
