package th.in.santi.data.datasource;

import io.reactivex.Observable;
import th.in.santi.domain.interactor.BallsUseCase;

/**
 * Created by Santi Tepinjai on 14-May-2017.
 *
 * @see {https://bitbucket.org/SantiDev/scb-assignment}
 */

public interface BallsDataSource {
    Observable<BallsUseCase.ResponseValues> operateBalls(BallsUseCase.RequestValues requestValues);

}
