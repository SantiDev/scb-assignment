package th.in.santi.data.repository;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import th.in.santi.data.datasource.BallsDataStoreFactory;
import th.in.santi.domain.interactor.BallsUseCase;
import th.in.santi.domain.repository.BallsRepository;

/**
 * Created by Santi Tepinjai on 14-May-2017.
 *
 * @see {https://bitbucket.org/SantiDev/scb-assignment}
 */

@Singleton
public class BallsDataRepository implements BallsRepository {

    private Context context;
    private BallsDataStoreFactory ballsDataStoreFactory;

    @Inject
    public BallsDataRepository(Context context, BallsDataStoreFactory userDataStoreFactory) {
        this.context = context;
        this.ballsDataStoreFactory = userDataStoreFactory;

    }

    @Override
    public Observable<BallsUseCase.ResponseValues> operateBalls(BallsUseCase.RequestValues requestValues) {
        return ballsDataStoreFactory.operateBalls(requestValues);
    }

}
