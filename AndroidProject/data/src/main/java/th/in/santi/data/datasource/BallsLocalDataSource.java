package th.in.santi.data.datasource;

import android.content.Context;
import android.util.Log;

import java.util.List;

import io.reactivex.Observable;
import th.in.santi.domain.interactor.BallsUseCase;

/**
 * Created by Santi Tepinjai on 14-May-2017.
 *
 * @see {https://bitbucket.org/SantiDev/scb-assignment}
 */

public class BallsLocalDataSource implements BallsDataSource {

    private final Context context;

    public BallsLocalDataSource(Context context) {
        this.context = context;
    }

    @Override
    public Observable<BallsUseCase.ResponseValues> operateBalls(BallsUseCase.RequestValues requestValues) {

        int[] balls = new int[requestValues.getBallsCount()];
        for (int i = 0; i < requestValues.getBallsCount(); i++) {
            balls[i] = i+1;
        }


        int k = requestValues.getK();
        List<int[]> abData = requestValues.getAbDataList();
        BallsUseCase.ResponseValues responseValue = new BallsUseCase.ResponseValues();
        while (k-- > 0) {
            for (int[] ab : abData) {
                int a = ab[0] - 1;
                int b = ab[1] - 1;

                int a_tmp = balls[a];
                balls[a] = balls[b];
                balls[b] = a_tmp;
            }
        }
        responseValue.setBalls(balls);
        return Observable.just(responseValue);
    }


}
