package th.in.santi.scb.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import th.in.santi.data.executor.JobExecutor;
import th.in.santi.data.repository.BallsDataRepository;
import th.in.santi.domain.executor.PostExecutionThread;
import th.in.santi.domain.executor.ThreadExecutor;
import th.in.santi.domain.interactor.BallsUseCase;
import th.in.santi.domain.repository.BallsRepository;
import th.in.santi.scb.app.AndroidApplication;
import th.in.santi.scb.ui.balls.BallsContract;
import th.in.santi.scb.ui.balls.BallsPresenter;
import th.in.santi.scb.ui.base.UIThread;

/**
 * Created by Santi Tepinjai on 14-May-2017.
 *
 * @see {https://bitbucket.org/SantiDev/scb-assignment}
 */

@Module
public class ApplicationModules {
    private final AndroidApplication application;

    public ApplicationModules(AndroidApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return this.application;
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }

    @Provides
    @Singleton
    ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides
    @Singleton
    BallsRepository provideBallsRepository(BallsDataRepository ballsDataRepository) {
        return ballsDataRepository;
    }



}
