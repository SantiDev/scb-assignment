package th.in.santi.scb.ui.balls;

import java.util.List;

import th.in.santi.scb.ui.base.BasePresenter;

/**
 * Created by Santi Tepinjai on 14-May-2017.
 *
 * @see {https://bitbucket.org/SantiDev/scb-assignment}
 */

public interface BallsContract {
    final String INPUT_N_K_REGEX = "([1-9]|([1-4]\\d)|50)([ ])([1-9]|\\d{2,9}|1000000000)";
    final String INPUT_A_B_REGEX = "[1-8][ ][1-8]";

    interface View {

        void showOutput(int[] balls);

        void showInputError();

        void showOperateError();
    }

    interface Presenter extends BasePresenter<View> {

        void operateBalls(String nkData, String abDataMultiLine);
    }
}
