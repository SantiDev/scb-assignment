package th.in.santi.scb.ui.base;

/**
 * Created by Santi Tepinjai on 14-May-2017.
 *
 * @see {https://bitbucket.org/SantiDev/scb-assignment}
 */


public interface BasePresenter<T> {
    void attachView(T view);

    /**
     * (Activity or Fragment) onResume() method.
     */
    void onResume();

    /**
     * (Activity or Fragment) onPause() method.
     */
    void onPause();

    /**
     * (Activity or Fragment) onDestroy() method.
     */
    void onDestroy();
}
