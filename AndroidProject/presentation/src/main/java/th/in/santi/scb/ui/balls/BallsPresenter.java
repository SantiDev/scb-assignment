package th.in.santi.scb.ui.balls;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import th.in.santi.domain.executor.PostExecutionThread;
import th.in.santi.domain.interactor.BallsUseCase;
import th.in.santi.scb.components.di.PerActivity;
import th.in.santi.scb.ui.base.UIThread;

/**
 * Created by Santi Tepinjai on 14-May-2017.
 *
 * @see {https://bitbucket.org/SantiDev/scb-assignment}
 */


@PerActivity
public class BallsPresenter implements BallsContract.Presenter {

    @Inject
    BallsUseCase ballsUseCase;

    @Inject
    PostExecutionThread uiThread;

    @Inject
    public BallsPresenter() {
    }

    private BallsContract.View view;

    private final int MAX_BALL_COUNT = 8;

    @Override
    public void attachView(BallsContract.View view) {
        this.view = view;
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onDestroy() {
        this.ballsUseCase.dispose();
        this.view = null;

    }

    private boolean isEmpty(String str) {
        return str == null || str.trim().length() == 0;
    }

    @Override
    public void operateBalls(String nkData, String abDataMultiLine) {

        if (isEmpty(nkData) || !nkData.matches(BallsContract.INPUT_N_K_REGEX)) {
            view.showInputError();
            return;
        }

        if (isEmpty(abDataMultiLine)) {
            view.showInputError();
            return;
        }

        List<int[]> abDataList = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new StringReader(abDataMultiLine));
        try {
            String abData;
            while (!isEmpty(abData = reader.readLine())) {
                if (!abData.matches(BallsContract.INPUT_A_B_REGEX)) {
                    view.showInputError();
                    return;
                }
                String[] tmp = abData.split(" ");
                int A = Integer.parseInt(tmp[0]);
                int B = Integer.parseInt(tmp[1]);
                if (A == B) {
                    view.showInputError();
                    return;
                }

                abDataList.add(new int[]{A, B});
            }
        } catch (IOException e) {
            view.showInputError();
            return;
        }


        String[] tmp = nkData.split(" ");
        int N = Integer.parseInt(tmp[0]);
        int K = Integer.parseInt(tmp[1]);
        BallsUseCase.RequestValues requestValues = new BallsUseCase.RequestValues();
        requestValues.setBallsCount(MAX_BALL_COUNT);
        requestValues.setN(N);
        requestValues.setK(K);
        requestValues.setAbDataList(abDataList);

        ballsUseCase.execute(requestValues, new BallsObserver());
    }


    final class BallsObserver extends DisposableObserver<BallsUseCase.ResponseValues> {

        @Override
        public void onNext(@NonNull BallsUseCase.ResponseValues requestValue) {
            view.showOutput(requestValue.getBalls());
        }

        @Override
        public void onError(@NonNull Throwable e) {
            view.showOperateError();
        }


        @Override
        public void onComplete() {
        }
    }
}
