package th.in.santi.scb.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import th.in.santi.scb.ui.balls.BallsContract;
import th.in.santi.scb.ui.balls.BallsPresenter;

/**
 * Created by Santi Tepinjai on 14-May-2017.
 *
 * @see {https://bitbucket.org/SantiDev/scb-assignment}
 */

@Module
public class PresenterModule {
    @Provides
    @Singleton
    BallsContract.Presenter provideBallsPresenter() {
        return new BallsPresenter();
    }

}
