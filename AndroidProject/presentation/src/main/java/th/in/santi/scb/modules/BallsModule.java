package th.in.santi.scb.modules;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import th.in.santi.domain.interactor.BallsUseCase;
import th.in.santi.scb.components.di.PerActivity;
import th.in.santi.scb.ui.balls.BallsContract;
import th.in.santi.scb.ui.balls.BallsPresenter;

/**
 * Created by Santi Tepinjai on 14-May-2017.
 *
 * @see {https://bitbucket.org/SantiDev/scb-assignment}
 */


@Module
public class BallsModule {
    @Provides
    @PerActivity
    @Named("ballsUseCase")
    BallsUseCase provideBallsUseCase(BallsUseCase ballsUseCase) {
        return ballsUseCase;
    }

    @Provides
    @PerActivity
    BallsContract.Presenter provideBallsPresenter(BallsPresenter ballsPresenter) {
        return ballsPresenter;
    }

}
