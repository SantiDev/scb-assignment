package th.in.santi.scb.ui.main;

import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import th.in.santi.scb.R;
import th.in.santi.scb.ui.base.BaseActivity;

/**
 * @author Santi Tepinjai
 * @see {https://bitbucket.org/SantiDev/scb-assignment}
 */

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeToolber();
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentContainer, MainFragment.newInstance())
                    .commit();

        }

    }

    private void initializeToolber(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setTitle(R.string.question1);
    }
}
