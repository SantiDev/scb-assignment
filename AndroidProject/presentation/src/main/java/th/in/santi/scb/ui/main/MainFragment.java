package th.in.santi.scb.ui.main;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v4.app.Fragment;
import android.view.View;

import th.in.santi.scb.R;
import th.in.santi.scb.databinding.FragmentMainBinding;
import th.in.santi.scb.ui.balls.BallsActivity;
import th.in.santi.scb.ui.base.BaseFragment;

/**
 * Created by Santi Tepinjai on 14-May-2017.
 *
 * @see {https://bitbucket.org/SantiDev/scb-assignment}
 */


public class MainFragment extends BaseFragment {
    public static Fragment newInstance() {
        return new MainFragment();
    }

    FragmentMainBinding binding;

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_main;
    }

    @Override
    protected void initializeView(View view) {
        binding = DataBindingUtil.bind(view);
        binding.setMainFragment(this);
    }

    public void onButtonNextClick(View view) {
        Intent intent = new Intent(getContext(), BallsActivity.class);
        startActivity(intent);
    }


}
