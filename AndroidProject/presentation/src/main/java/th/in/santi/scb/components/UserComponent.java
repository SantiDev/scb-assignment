package th.in.santi.scb.components;

import dagger.Component;
import th.in.santi.scb.components.di.PerActivity;
import th.in.santi.scb.modules.ActivityModule;
import th.in.santi.scb.modules.BallsModule;
import th.in.santi.scb.ui.balls.BallsFragment;

/**
 * Created by Santi Tepinjai on 14-May-2017.
 *
 * @see {https://bitbucket.org/SantiDev/scb-assignment}
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, BallsModule.class})
public interface UserComponent extends ActivityComponent {
    void inject(BallsFragment baseFragment);
}

