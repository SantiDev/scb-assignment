package th.in.santi.scb.components;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import th.in.santi.domain.executor.PostExecutionThread;
import th.in.santi.domain.executor.ThreadExecutor;
import th.in.santi.domain.repository.BallsRepository;
import th.in.santi.scb.modules.ApplicationModules;
import th.in.santi.scb.ui.base.BaseActivity;

/**
 * Created by Santi Tepinjai on 14-May-2017.
 *
 * @see {https://bitbucket.org/SantiDev/scb-assignment}
 */

@Singleton

@Component(modules = {ApplicationModules.class})
public interface ApplicationComponent {

    void inject(BaseActivity baseActivity);

    Context context();

    ThreadExecutor threadExecutor();

    PostExecutionThread postExecutionThread();

    BallsRepository ballsRepository();

}
