package th.in.santi.scb.app;

import android.app.Application;

import th.in.santi.scb.components.ApplicationComponent;
import th.in.santi.scb.components.DaggerApplicationComponent;
import th.in.santi.scb.modules.ApplicationModules;

/**
 * Created by Santi Tepinjai on 14-May-2017.
 *
 * @see {https://bitbucket.org/SantiDev/scb-assignment}
 */

public class AndroidApplication extends Application {
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        this.applicationComponent = DaggerApplicationComponent.builder()
                .applicationModules(new ApplicationModules(this))
                .build();

    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
