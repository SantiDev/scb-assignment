package th.in.santi.scb.ui.balls;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import javax.inject.Inject;

import th.in.santi.scb.R;
import th.in.santi.scb.components.UserComponent;
import th.in.santi.scb.databinding.FragmentBallsBinding;
import th.in.santi.scb.ui.base.BaseFragment;

/**
 * Created by Santi Tepinjai on 14-May-2017.
 *
 * @see {https://bitbucket.org/SantiDev/scb-assignment}
 */


public class BallsFragment extends BaseFragment implements BallsContract.View {


    public static Fragment newInstance() {
        return new BallsFragment();
    }


    @Inject
    BallsContract.Presenter ballsPresenter;

    FragmentBallsBinding binding;
    private Toast toast;
    int N, K;


    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_balls;
    }

    @Override
    protected void initializeView(View view) {
        UserComponent userComponent = getComponent(UserComponent.class);
        userComponent.inject(this);
        binding = DataBindingUtil.bind(view);
        binding.setBallsFragment(this);
        ballsPresenter.attachView(this);
        binding.editTextAB.setEnabled(false);
        binding.editTextNK.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (binding.linearLayoutOutput.getVisibility() == View.VISIBLE)
                    binding.linearLayoutOutput.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {
                String str = s.toString().trim();
                if (str.indexOf("  ") > 0) {
                    str = str.replaceAll("  ", " ");
                    binding.editTextNK.setText(str);
                    binding.editTextNK.setSelection(binding.editTextAB.length());
                }

                if (str.length() > 0 && str.matches(BallsContract.INPUT_N_K_REGEX)) {
                    String[] tmp = str.replaceAll("  ", " ").split(" ");
                    N = Integer.parseInt(tmp[0]);
                    K = Integer.parseInt(tmp[1]);
                    binding.editTextAB.setEnabled(true);
                } else {
                    N = K = 0;
                    if (binding.editTextAB.getText().length() > 0) {
                        binding.editTextAB.setText("");
                    }
                    binding.editTextAB.setEnabled(false);
                    if (str.indexOf(" ") > 0 && !str.endsWith(" ")) {
                        showInputError();
                    }
                }
            }
        });

        binding.editTextAB.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (binding.linearLayoutOutput.getVisibility() == View.VISIBLE)
                    binding.linearLayoutOutput.setVisibility(View.INVISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {
                int line = binding.editTextAB.getLineCount();
                String str = s.toString().trim();
                if (str.indexOf("  ") > 0) {
                    str = str.replaceAll("  ", " ");
                    binding.editTextAB.setText(str);
                    binding.editTextAB.setSelection(binding.editTextAB.length());
                }

                if (str.indexOf("\n\n") > 0) {
                    str = str.replaceAll("\n\n", "\n");
                    binding.editTextAB.setText(str);
                    binding.editTextAB.setSelection(binding.editTextAB.length());
                }

                if (line - 1 == N) {
                    binding.buttonOperate.requestFocus();
                    hideKeyboard(binding.editTextAB.getWindowToken());
                    if (str.endsWith("\n")) {
                        binding.editTextAB.setText(str.substring(0, str.length() - 1));
                    }
                }
            }
        });
    }

    private void hideKeyboard(IBinder token) {
        InputMethodManager manager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.hideSoftInputFromWindow(token, 0);
    }

    public void onOperateBallsButtonClick(View view) {
        binding.linearLayoutOutput.setVisibility(View.INVISIBLE);
        String nkData = binding.editTextNK.getText().toString();
        String abData = binding.editTextAB.getText().toString();
        ballsPresenter.operateBalls(nkData, abData);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ballsPresenter.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        ballsPresenter.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        ballsPresenter.onResume();
    }


    @Override
    public void showOutput(int[] balls) {
        binding.linearLayoutOutput.setVisibility(View.VISIBLE);
        String text = "";
        for (int ball : balls) {
            text += ball + " ";
        }
        binding.outputData.setText(text);
    }

    @Override
    public void showInputError() {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(getContext(), R.string.error_input, Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void showOperateError() {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(getContext(), R.string.error_operate, Toast.LENGTH_SHORT);
        toast.show();
    }
}
