package th.in.santi.domain.interactor;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import th.in.santi.domain.executor.PostExecutionThread;
import th.in.santi.domain.executor.ThreadExecutor;
import th.in.santi.domain.repository.BallsRepository;

/**
 * Created by Santi Tepinjai on 14-May-2017.
 *
 * @see {https://bitbucket.org/SantiDev/scb-assignment}
 */

public class BallsUseCase extends UseCase<BallsUseCase.RequestValues, BallsUseCase.ResponseValues> {

    private final BallsRepository ballsRepository;

    @Inject
    protected BallsUseCase(ThreadExecutor threadExecutor,
                           PostExecutionThread postExecutionThread,
                           BallsRepository ballsRepository) {
        super(threadExecutor, postExecutionThread);
        this.ballsRepository = ballsRepository;
    }

    @Override
    protected Observable<ResponseValues> buildUseCaseObservable(RequestValues requestValues) {
        return ballsRepository.operateBalls(requestValues);
    }

    public static class RequestValues implements UseCase.RequestValues {
        private int ballsCount;
        private int N;
        private int K;

        public int getBallsCount() {
            return ballsCount;
        }

        public void setBallsCount(int ballsCount) {
            this.ballsCount = ballsCount;
        }

        private List<int[]> abDataList;

        public int getN() {
            return N;
        }

        public void setN(int n) {
            N = n;
        }

        public int getK() {
            return K;
        }

        public void setK(int k) {
            K = k;
        }

        public List<int[]> getAbDataList() {
            return abDataList;
        }

        public void setAbDataList(List<int[]> abDataList) {
            this.abDataList = abDataList;
        }
    }

    public static class ResponseValues implements UseCase.ResponseValues {
        private int[] balls;

        public int[] getBalls() {
            return balls;
        }

        public void setBalls(int[] balls) {
            this.balls = balls;
        }
    }
}
