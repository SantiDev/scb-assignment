package th.in.santi.domain.repository;

import io.reactivex.Observable;
import th.in.santi.domain.interactor.BallsUseCase;

/**
 * Created by Santi Tepinjai on 14-May-2017.
 *
 * @see {https://bitbucket.org/SantiDev/scb-assignment}
 */

public interface BallsRepository {

    Observable<BallsUseCase.ResponseValues> operateBalls(BallsUseCase.RequestValues requestValues);
}
