package th.in.santi.scb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 * @author Santi Tepinjai
 * @see {@link https://bitbucket.org/SantiDev/scb-assignment}
 *
 */
public class Question1Test {
	private static Question1Inf question1;

	@BeforeClass
	public static void initQuestion1() {
		question1 = new Question1();
	}

	@Before
	public void beforTest() {
	}

	@After
	public void afterTest() {
	}

	@Test
	public void testNKDataFormat() {
		String input = "1 1";
		assertTrue(input, question1.isNKDataFormat(input));

		input = "0 1";
		assertTrue(input, !question1.isNKDataFormat(input));

		input = "1 1000000000";
		assertTrue(input, question1.isNKDataFormat(input));

		input = "1 1000000001";
		assertTrue(input, !question1.isNKDataFormat(input));

		input = "1 999999999";
		assertTrue(input, question1.isNKDataFormat(input));

		input = "51 1000000000";
		assertTrue(input, !question1.isNKDataFormat(input));

		input = "49 1000000001";
		assertTrue(input, !question1.isNKDataFormat(input));

		input = "1 200";
		assertTrue(input, question1.isNKDataFormat(input));

		input = "-1 500";
		assertTrue(input, !question1.isNKDataFormat(input));
	}

	@Test
	public void testABDataFormat() {
		String input = "1 1";
		assertTrue(input, question1.isABDataFormat(input));

		input = "0 1";
		assertTrue(input, !question1.isABDataFormat(input));

		input = "9 1";
		assertTrue(input, !question1.isABDataFormat(input));

		input = "1 9";
		assertTrue(input, !question1.isABDataFormat(input));

		input = "9 9";
		assertTrue(input, !question1.isABDataFormat(input));

		input = "10 0";
		assertTrue(input, !question1.isABDataFormat(input));

		input = "3 1";
		assertTrue(input, question1.isABDataFormat(input));

		input = "2 5";
		assertTrue(input, question1.isABDataFormat(input));
	}

	@Test
	public void testOperations() {

		List<int[]> abData = new ArrayList<>();
		abData.add(new int[] { 1, 2 });
		abData.add(new int[] { 2, 3 });
		abData.add(new int[] { 3, 4 });
		abData.add(new int[] { 4, 1 });

		int k = 2;
		int[] balls = question1.getDefaultBalls();
		question1.operations(balls, abData, k);
		String ballString = "";
		for (int ball : balls) {
			ballString += ball;
		}
		assertEquals("14235678", ballString);

		abData.clear();
		abData.add(new int[] { 1, 3 });
		abData.add(new int[] { 6, 8 });
		abData.add(new int[] { 3, 5 });
		abData.add(new int[] { 2, 6 });
		abData.add(new int[] { 3, 7 });
		abData.add(new int[] { 3, 4 });
		abData.add(new int[] { 4, 7 });
		abData.add(new int[] { 2, 4 });
		abData.add(new int[] { 1, 3 });
		abData.add(new int[] { 2, 7 });
		abData.add(new int[] { 2, 7 });
		abData.add(new int[] { 2, 4 });
		abData.add(new int[] { 6, 7 });
		abData.add(new int[] { 1, 7 });
		abData.add(new int[] { 3, 4 });
		abData.add(new int[] { 1, 6 });

		k = 1000000000;
		balls = question1.getDefaultBalls();
		question1.operations(balls, abData, k);
		
		ballString = "";
		for (int ball : balls) {
			ballString += ball;
		}
		assertEquals("18345276", ballString);
	}
}
