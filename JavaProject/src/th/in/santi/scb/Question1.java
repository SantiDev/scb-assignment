package th.in.santi.scb;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 
 * @author Santi Tepinjai
 * @see {@link https://bitbucket.org/SantiDev/scb-assignment}
 *
 */
public class Question1 implements Question1Inf {

	private final String INPUT_N_K_REGEX = "([1-9]|([1-4]\\d)|50)([ ])([1-9]|\\d{2,9}|1000000000)";
	private final String INPUT_A_B_REGEX = "[1-8][ ][1-8]";
	private final int MAX_INVALID_INPUT = 3;
	private Scanner scanner;

	@Override
	public boolean isNKDataFormat(String input) {
		return input.matches(INPUT_N_K_REGEX);
	}

	@Override
	public boolean isABDataFormat(String input) {
		return input.matches(INPUT_A_B_REGEX);
	}

	@Override
	public void operations(int[] balls, List<int[]> abData, int k) {
		while (k-- > 0) {
			for (int[] ab : abData) {
				int a = ab[0] - 1;
				int b = ab[1] - 1;
				int a_tmp = balls[a];
				balls[a] = balls[b];
				balls[b] = a_tmp;
			}
		}
	}

	private void openScanner() {
		scanner = new Scanner(System.in);
	}

	private void closeScanner() {
		if (scanner != null) {
			scanner.close();
			scanner = null;
		}
	}

	private void showDesc() {
		StringBuilder builder = new StringBuilder();
		builder.append("Given 8 balls aligned horizontally and numbered 1, 2, 3, 4, 5, 6, 7, 8 from left to right.");
		builder.append("\nWe are going to perform the following N operations in the given order.");
		builder.append("\n\t● Exchange A_1th ball from the left and B_1th ball from the left.");
		builder.append("\n\t● Exchange A_2th ball from the left and B_2th ball from the left.");
		builder.append("\n\t● :");
		builder.append("\n\t● Exchange A_Nth ball from the left and B_Nth ball from the left.");
		builder.append("\nThis series of N operations stated above will be one set.");
		builder.append("\nYour task is to output the ultimate arrangement of these balls after repeating K sets.");
		builder.append("\n\n--------------------------------------------------------------------------------------\n");
		System.out.println(builder);
	}

	private void showBeginInput() {
		StringBuilder builder = new StringBuilder();
		builder.append("\nInput will be given in the following format from Standard Input:");
		builder.append("\nN K");
		builder.append("\nA_1 B_1");
		builder.append("\nA_2 B_2");
		builder.append("\n:");
		builder.append("\nA_N B_N\n");
		builder.append("\n---------------------------------------------------------------------------------------");
		builder.append("\nN is between 1 and 50.");
		builder.append("\nK is between 1 and 1000000000.");
		builder.append("\nA and B is between 1 and 8.");
		builder.append("\n----------------------------------Begin input------------------------------------------\n");
		System.out.println(builder);
	}

	private void showEndInput() {
		System.out.println("----------------------------------End input------------------------------------------");
	}

	private void showtOutput(int[] balls) {
		System.out.print("Output : ");
		for (int ball : balls) {
			System.out.print(ball + " ");
		}
		System.out.println("\n\n");
	}

	private int[] startInputNK() {
		int n = 0, k = 0, i = MAX_INVALID_INPUT;
		while (i > 0) {
			String input = scanner.nextLine();
			if (isNKDataFormat(input)) {
				String[] nk = input.split(" ");
				n = Integer.parseInt(nk[0]);
				k = Integer.parseInt(nk[1]);
				break;
			}

			System.out.println("[" + i + "] Invarid input format!");
			i--;
		}

		if (n == 0 && k == 0) {
			System.out.println("You have entered too many invalid data.");
			System.exit(0);
		}

		return new int[] { n, k };

	}

	private List<int[]> startInputAB(int n) {
		List<int[]> abData = new ArrayList<>();
		int a = 0, b = 0, i = MAX_INVALID_INPUT;
		while (n-- > 0) {
			while (i > 0) {
				String input = scanner.nextLine();
				if (isABDataFormat(input)) {
					String[] nk = input.split(" ");
					a = Integer.parseInt(nk[0]);
					b = Integer.parseInt(nk[1]);
					if (a != b) {
						break;
					}
				}

				System.out.println("[" + i + "] Invarid input format!");
				i--;
			}
			if (a == 0 && b == 0) {
				System.out.println("You have entered too many invalid data.");
				System.exit(0);
			}
			abData.add(new int[] { a, b });
		}

		return abData;
	}

	@Override
	public void startApp() {
		showDesc();
		showBeginInput();
		openScanner();
		int[] balls = getDefaultBalls();
		int[] nkData = startInputNK();
		List<int[]> abData = startInputAB(nkData[0]);
		closeScanner();
		showEndInput();

		operations(balls, abData, nkData[1]);
		showtOutput(balls);
	}

	public static void main(String[] args) {
		Question1Inf question1 = new Question1();
		question1.startApp();
	}
}
