package th.in.santi.scb;

import java.util.List;

/**
 * 
 * @author Santi Tepinjai
 * @see {@link https://bitbucket.org/SantiDev/scb-assignment}
 *
 */
public interface Question1Inf {

	public default int[] getDefaultBalls() {
		return new int[] { 1, 2, 3, 4, 5, 6, 7, 8 };
	}
	public void startApp();
	
	public boolean isNKDataFormat(String input);

	public boolean isABDataFormat(String input);

	public void operations(int[] balls, List<int[]> abData, int k);
}
